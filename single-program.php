<!--プログラム投稿機能始まり-戸野-->
<div class="post-element program col-xs-12 col-md-5">
	<div class="post-element-header">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon_info2.png"></img>
		<h3 class = "info2_header">プログラム案内</h3>
	</div>
    <dl class = "info2">
    	<?php $args = array(
        	'numberposts' => 3,                //表示（取得）する記事の数
        	'post_type' => 'program'    //投稿タイプの指定
    	);
    	$posts = get_posts( $args );
    	if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
    		<dt class="post-date"><?php the_time("Y.n.j"); ?></dt>
        	<dd><?php the_content(); ?></dd>
    	<?php endforeach; ?>
    	<?php else : //記事が無い場合 ?>
        	<li><p>記事はまだありません。</p></li>
    	<?php endif;
    	wp_reset_postdata(); //クエリのリセット ?>
    </dl>
</div>
<!--プログラム投稿機能終わり-戸野-->
