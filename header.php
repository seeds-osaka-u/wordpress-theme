<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<!-- bootstrapの読み込み 花屋-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<!--  -->
<!--cssのキャッシュの反映-戸野-->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" />
<!---->
<?php wp_head(); ?>

<!-- 追加 -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

<!--jsファイルの読み込み-戸野-->
<?php wp_enqueue_script('site', get_stylesheet_directory_uri(). '/js/site.js', array('jquery'), '1.0' ); ?>
<!---->
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>
		<div class="header-container">
			<header id="masthead" class="site-header" role="banner">
				<!--seedsHPのheaderのコード始まり-戸野-->
				<div id="header" class="container">
					<div class="inner row">
						<div id="header-logo" class="col-xl-2">
							<a href="index.php">
								<img id="seeds-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt="seeds logo" width="170px" height="145px" />
							</a>
						</div>
						<div class="header-titles col-xl-7">
							<h2>大阪大学グローバルサイエンスキャンパス</h2>
						<!-- </div>
						<div class="header-titles col-xl-4"> -->
							<h2 id="header-description">世界適塾の教育研究力を活かしたSEEDSプログラム<br />
							～傑出した科学技術人材発見と早期育成～
							</h2>
						</div>
						<div id="header-sns" class="col-xl-1">
							<a href="https://www.facebook.com/大阪大学-SEEDS-1746503512238521/" target="_blank"><i class="fab fa-facebook fb_logo fa-4x my-blue"/></i></a>
							<a href="https://twitter.com/OSAKASEEDS" target="_blank"><i class="fab fa-twitter tw_logo fa-4x my-skyblue"/></i></a>
						</div>
						<div id="osakauni-outer" class="col-xl-2">
							<a href="http://www.osaka-u.ac.jp/ja" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_osakauni.png" alt="大阪大学ロゴ" border="0" /></a>
						</div>
					</div>
				</div>
				<nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="top-navbar">
					<!-- <a class="navbar-brand" href="#">メニュー</a> -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
						<li class="nav-item active top">
							<a class="nav-link" href="#">トップ <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item about">
							<a class="nav-link" href="#about">SEEDSとは</a>
						</li>
						<li class="nav-item taikan">
							<a class="nav-link" href="http://seeds.celas.osaka-u.ac.jp/taikan_course/index.html" target="_blank">体感コース</a>
						</li>
						<li class="nav-item jikkan">
							<a class="nav-link" href="http://seeds.celas.osaka-u.ac.jp/jikkan_course/index.html" target="_blank">実感コース</a>
						</li>
						<li class="nav-item invitation">
							<a class="nav-link" href="http://seeds.celas.osaka-u.ac.jp/registration/index.html" target="_blank">募集情報</a>
						</li>
						<li class="nav-item faq">
							<a class="nav-link" href="http://seeds.celas.osaka-u.ac.jp/faq/index.html" target="_blank">よくある質問</a>
						</li>
						<!-- <li class="nav-item recruit">
							<a class="nav-link" href="#">採用情報</a>
						</li> -->
						<!-- <li class="nav-item contact">
							<a class="nav-link" href="#">お問い合わせ・アクセス</a>
						</li> -->
						<!-- <li class="nav-item relative-links">
							<a class="nav-link" href="#">関連リンク</a>
						</li> -->
						<li class="nav-item students_only">
							<a class="nav-link" href="http://seeds.celas.osaka-u.ac.jp/members/index.html" target="_blank">受講生専用ページ</a>
						</li>
					</div>
				</nav>
			</header><!-- #masthead -->
		</div>

	<?php

	/*
	* If a regular post or page, and not the front page, show the featured image.
	* Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	*/
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
