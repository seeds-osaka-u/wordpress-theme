<?php get_header(); ?>
<div id="contents">
<h3 class="about_header">体感コース</h3>
<!-- <div class="box1">
2015年度体感科学研究は終了いたしました。
</div> -->
<h4>ファーストステップ体感コース</h4>
<h5>体感科学技術・体感国際交流</h5>
<ul>
<li><strong>内容</strong><br />
オムニバス形式の講義と講義後の「めばえ道場」（少人数によるディスカッション）や 最先端科学技術体験ツアー等と留学生との交流など異文化交流体験。
</li><br>
<li><strong>日程</strong><br />
<a href="sched2018.html"> 2018年度</a> &nbsp
<a href="sched2017.html"> 2017年度</a> &nbsp
<a href="sched2016.html"> 2016年度</a> &nbsp
<a href="sched2015.html"> 2015年度</a>
</li><br>
<!--
<br /><br />
<a href="2016体感技術国際時間割案0506.pdf"><img src="taikan_sched_160506.png" width="690px" height="500px" alt="予定（案）"/></a>
<br /><br /><br />
2018年度： <a href= sched2018.html#theme> 研究テーマ一覧(準備中) </a> <br>
2018年度： 研究テーマ一覧(準備中) <br>
2018年8月～2019年1月の期間内の土曜日に6回程度<br>
(期日、日数は研究テーマによって異なります。)
<br>
<br>
-->
<ul>
<h5>体感科学研究</h5>
<li><strong>内容</strong><br />
物質系、生命系、数物系、応用技術系、情報・数理系の各コースのさまざまな研究や体験参加</li><br>
<li><strong>日程</strong><br />
2018年度： <a href= theme2018.html> 研究テーマ一覧 </a> <br>
2017年度： <a href= theme2017.html> 研究テーマ一覧 </a> <br>
2016年度： <a href= theme2016.html> 研究テーマ一覧 </a> <br>
2015年度： <a href= theme2015.html> 研究テーマ一覧 </a> <br/>
</li><br>
</ul>

<?php get_footer();
