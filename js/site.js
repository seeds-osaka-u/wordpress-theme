// グローバルメニューのドロップダウン追加始まり-戸野
jQuery(function ($) {
  $('.globalnav li').hover(function() {
    // 表示しているメニューを閉じる
    $('.globalnav ').removeClass('selected');
    $('.globalnav-child').hide();

    // メニュー表示
    if ($(this).attr('class') == 'taikan'){
      $(this).addClass('selected');
      var html = '<ul class="globalnav-child"><li><a href="#">2017</a></li><li><a href="#">2016</a></li><li><a href="#">2015</a></li></ul>'
      $(".selected").append(html).slideDown('fast');
    }
    else if ($(this).attr('class') == 'zikkan'){
      $(this).addClass('selected');
      var html = '<ul class="globalnav-child"><li><a href="#">2017</a></li><li><a href="#">2016</a></li></ul>'
      $(".selected").append(html).slideDown('fast');
    }
    else if ($(this).attr('class') == 'news'){
      $(this).addClass('selected');
      var html = '<ul class="globalnav-child"><li><a href="#">ニュースと活動報告</a></li><li><a href="#">ニュースレター</a></li></ul>'
      $(".selected").append(html).slideDown('fast');
    }
    else if ($(this).attr('class') == 'question'){
      $(this).addClass('selected');
      var html = '<ul class="globalnav-child"><li><a href="#">よくある質問</a></li><li><a href="#">お問い合わせ・アクセス</a></li></ul>'
      $(".selected").append(html).slideDown('fast');
    }
    else if ($(this).attr('class') == 'senyopage'){
      $(this).addClass('selected');
      var html = '<ul class="globalnav-child"><li><a href="#">2017</a></li><li><a href="#">2016</a></li><li><a href="#">2015</a></li></ul>'
      $(".selected").append(html).slideDown('fast');
    }
  });

  // マウスカーソルがメニュー上/メニュー外
  $('.globalnav li,ul').hover(function(){
    over_flg = true;
  }, function(){
    over_flg = false;
  });

  // メニュー領域外をクリックしたらメニューを閉じる
  $('body').click(function() {
    if (over_flg == false) {
      $('.globalnav li').removeClass('selected');
      $('.globalnav-child').slideUp('fast');
    }
  });
});
// グローバルメニューのドロップダウン追加終わり-戸野
