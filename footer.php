		</div><!-- #content -->
		<div class="footer-container">
			<div id="footer_links">
				<div class="inner">
					<div class="blocks1">
						<h3>メニュー</h3>
						<ul>
							<li><a href="index.php">トップ</a></li>
							<li><a href="index.php#about">Seedsとは</a></li>
							<li><a href="http://seeds.celas.osaka-u.ac.jp/taikan_course/index.html" target="_blank">体感コース</a></li>
							<li><a href="http://seeds.celas.osaka-u.ac.jp/jikkan_course/index.html" target="_blank">実感コース</a></li>
						</ul>
					</div>
					<div class="blocks2">
						<h3>&nbsp;</h3>
						<ul>
							<li><a href="http://seeds.celas.osaka-u.ac.jp/registration/index.html" target="_blank">募集情報</a></li>
							<li><a href="http://seeds.celas.osaka-u.ac.jp/faq/index.html" target="_blank">よくある質問</a></li>
							<li><a href="http://seeds.celas.osaka-u.ac.jp/members/index.html" target="_blank">受講生専用ページ</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div id="footer">
				<div class="inner">
				本プログラムは国立研究開発法人科学技術振興機構『グローバルサイエンスキャンパス』の委託事業です。<br />
				Copyright Reserved &copy; 大阪大学グローバルサイエンスキャンパス  Sekai-tekijuku Enhanced Education for Distinguished Students（SEEDS）
				</div>
			</div>
		</div>
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>
<!-- bootstrapの読み込み 花屋-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<!--  -->
</body>
</html>
