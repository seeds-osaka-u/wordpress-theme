<?php get_header(); ?>

<!-- <div class="wrap"> 変更 花屋-->
<div class="main-container">
	<?php if ( is_home() && ! is_front_page() ): ?>
		<header class="page-header">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</header>
	<?php else: ?>
        <header class="page-header">
            <h2 class="page-title"><?php _e( 'Posts', 'twentyseventeen' ); ?></h2>
        </header>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">      
            <!-- 追加 -->
            <!-- <div class="inner">
                <div class="main_students">
                    <img src="<?php // echo getstylesheete_directory_uri(); ?>/assets/images/students.png" alt="生徒たち">
                </div>
                <div class="main_message">
                    <h3>科学技術の大樹を目指そう。</h3>
                    <p>断片的な知識・技能を有機的に結びつけて課題に挑戦するという、研究の基本作法を高校生が学ぶのが大阪大学のSEEDSプログラムです。研究の経験がなくても大丈夫。種から育て、発芽を支援します。大樹を目指しましょう。</p>
                    <p class="textCenter"><span class="main_button"><a href="https://seeds.celas.osaka-u.ac.jp/registration/index.html">参加高校生募集</a></span></p>
                    <p class="smart_banner"><img src="<?php // echo get_stylesheet_directory_uri(); ?>/assets/images/smart_banner.png" alt=""></p>
                </div>
            </div> -->
            <!-- -->
			<!--トップページデザイン始まり-戸野-->
            
			<div class="contents">
				<!-- <div class="mainvisual">
                <?php // echo do_shortcode('[metaslider id="4"]'); ?>
				</div> -->
				<!--投稿機能コード始まり-->
				<div class="post container-fluid">
					<!--カスタム投稿タイプ(プログラム)記事一覧表示コード-->
						<?php get_template_part('single-program');?>
					<!--カスタム投稿タイプ(ニュースと活動案内)記事一覧表示コード-->
						<?php get_template_part('single-newsandactivities');?>
				</div>
				<!--投稿機能コード終わり-->
                <div class="aboutseeds" id="about">
                    <h2 class="section-header">SEEDSとは</h2>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="aboutseeds-element explanationofseeds col-md-6 col-lg-6">
                                <h4 id="aboutseeds-subtitle">世界適塾の研究力を活かしたSEEDSプログラム</br>〜傑出した科学技術人材発見と早期育成〜</h4>
                                <p><span>SEEDS </span>は「Sekai-tekijuku Enhanced Education for Distinguished Students」の頭文字をとったもので、世界最先端の科学技術にいち早く触れてみたいという意欲的な高校生向けのプログラムです。大阪大学での多岐にわたる研究に触れてもらうことで科学に対する小さな好奇心の芽を大きく伸ばしてもらうのが目的です。</br><span>SEEDS </span>は次のような高校生に参加していただくことを想定しています。</br></br>・SSHなどに参加されていない高校に在籍していて、個人として研究活動に意欲を持っていても実際にはどうしていいかわからない高校生</br>・SSH校などで実際に研究に参加されている高校生だけでなく、SSH校でも研究の機会のない高校生</br></br>
                                このような、これから伸びようとする人材(種：<span>SEEDS </span>)に、大学の研究に触れてもらうことで、研究に対する芽(目)を大きく伸ばしてもらうことが、<span>SEEDS </span>プログラムの目的です。</p>
                            </div>
                            <div class="aboutseeds-element movie col-md-6 col-lg-6">
                                <iframe width="450" height="350" src="https://www.youtube.com/embed/AnUMDIsOTeo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="aboutseeds-element">
                        <a href="../pdf/about/seeds_program180507.pdf" onclick="window.open(this.href,'new',''); return false" class="btn btn-outline-primary program-download-btn">プログラム概要はこちらから(ダウンロード)</a>
                    </div>
                </div>
                <div class="human">
                    <h2 class="section-header">育てたい人物像</h2>
                    <div class="ability kadaitankyu">
                        <h3>課題探求力</h3>
                        <p>広い基礎科学力と各分野の専門知識の獲得ができ、講義（コア科目）への参加による学力向上が期待でき、またオンデマンド講義による自主的・能動的な学習に積極的に取り組む。</p>
                    </div>
                    <div class="ability ronritekisiko">
                        <h3>論理的思考力</h3>
                        <p>科学的な考え方と問題解決方法の修得に優れ、研究室の少人数で実践される答えの得られていない問題に対する体験を通じて、大きくその能力を伸ばす。</p>
                    </div>
                    <div class="ability leadership">
                        <h3>リーダーシップ</h3>
                        <p>自主的な研究計画の作成、グループ研究体験（基礎セミナーやオナーセミナー）、異分野や留学生との交流に積極的に取り組む。</p>
                    </div>
                    <div class="ability presentation">
                        <h3>プレゼンテーション能力</h3>
                        <p>小論文やレポート作成によって、自主研究の結果整理と論点整理を行う資質に優れた者。また研究成果発表会等で自分の研究内容に対する専門家との質疑応答に積極的に取り組む。</p>
                    </div>
                    <div class="ability eigokoryu">
                        <h3>英語交流</h3>
                        <p>留学生との国際交流イベントや、海外からの学生や研究者と開催する研究会へ参加できる英語力を有する。</p>
                    </div>
                    <div class="ability kagakurinri">
                        <h3>科学者倫理</h3>
                        <p>プログラム開始前の「導入教育」で行われる倫理教育に関心をもち、積極的に取り組む。</p>
                    </div>
                </div>
                <!-- <div class="aboutprogram">
                    <h2 class="section-header">プログラム概要</h2>
                    <div class="programgaiyo">
                        <img src = "<?php echo get_stylesheet_directory_uri(); ?>/assets/images/HP-SEEDS.png"></img>
                    </div>
                </div> -->
                <div class="centering-box">
                    <a href="http://seeds.celas.osaka-u.ac.jp/registration/index.html" target="_blank" class="btn btn-outline-primary program-download-btn">参加高校生募集</a>
                </div>
                <div class="messagesfromteachers">
                    <h2 class="section-header">先生からのメッセージ</h2>
                    <a class="btn btn-light" data-toggle="collapse" href="#kikuchi" role="button" aria-expanded="false" aria-controls="kikuchi">
                    菊池誠先生
                    </a>
                    <a class="btn btn-light" data-toggle="collapse" href="#ishiguro" role="button" aria-expanded="false" aria-controls="ishiguro">
                    石黒浩先生
                    </a>
                    <a class="btn btn-light" data-toggle="collapse" href="#minamikata" role="button" aria-expanded="false" aria-controls="minamikata">
                    南方聖司先生
                    </a>
                    <a class="btn btn-light" data-toggle="collapse" href="#arakawa" role="button" aria-expanded="false" aria-controls="arakawa">
                    荒川智紀先生
                    </a>
                    <div class="kikuchi teachers collapse" id="kikuchi">
                        <div class="introduction container-fluid">
                            <div class="row">
                                <div class="teacherphoto col-md-2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/kikuchi.png"></div>
                                <div class="col-md-10">
                                    <p class="katagaki">大阪大学サイバーメディアセンター</p>
                                    <p class="teacher-name">菊池誠先生</p>
                                    <p>「科学とは『知識の集合』ではなく『考え方』です。」</p>
                                </div>
                            </div>
						</div>
                        <div class="message">
                            <p>世の中には「科学を装っているけども、実は科学ではない怪しげなモノ」（ニセ科学）が出回っています。効果のない代替医療や健康食品の数々、一昔前は「水からの伝言」も記憶に新しいですが、いわば科学の反面教師のようなものです。私はこれが「なぜ科学に見えてしまうのか」、なぜ人々の心を惹きつけるのか」ということに興味を持っています。
                            　科学には面白い事実がたくさんあります。しかし、知識だけでなく「科学的思考法」「科学的証明法」といった論理を身につけなければなりません。知識をただ並べていくだけでは本物と偽物の科学が区別できるのです。講義ではニセ科学についてスポットあてお話します。この機会にみなさんには「科学の論理」について考えて欲しいと思います。</p>
                        </div>
                    </div>
                    <div class="ishiguro teachers collapse" id="ishiguro">
                        <div class="introduction container-fluid">
                            <div class="row">
                                <div class="teacherphoto col-md-2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ishiguro.png"></div>
                                <div class="col-md-10">
                                    <p class="katagaki">大阪大学基礎工学研究科</p>
                                    <p class="teacher-name">石黒浩先生</p>
                                    <p>「『基礎的問題』にこそ疑問を持って欲しい。」</p>
                                </div>
                            </div>
						</div>
                        <div class="message"><p>これまで、日常的に人間と関わることができるロボットを目指して、数多くアンドロイドを開発してきた。アンドロイドはただ人間を模擬した機械というだけではない、「人間らしさとは何か」「自分とは何か」「心とは何か」という人間に関する基本的問題にも繋がるものである。より人間に近いアンドロイドをつくることを通して人間を理解できると考えている。講義では、アンドロイドを創った「発見」について紹介する。
                        　僕たちはいつも「自分の価値」について考えている。子供のころは、人間には絶対的な価値があるように教わる。しかしそうではなく、人間の価値は自分自身で探し出すものだと思っている。探し続けることをやめたら僕達の生きるモチベーションも失われてしまうはずだ。高校生の皆さんには、これから研究体験を深めてもらって、自分の価値・可能性を探しだしてもらいたい。また、あまり自分の目標を狭めることなく、幅広い視野をもって面白い研究に挑戦して欲しい。</p></div>
                    </div>
                    <div class="minamikata teachers collapse" id="minamikata">
                        <div class="introduction container-fluid">
                            <div class="row">
                                <div class="teacherphoto col-md-2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/minamikata.png"></div>
                                <div class="col-md-10">
                                    <p class="katagaki">大阪大学工学研究科</p>
                                    <p class="teacher-name">南方聖司先生</p>
                                    <p>「想像から創造に。」</p>
                                </div>
                            </div>
						</div>
                        <div class="message"><p>有機化学（炭素を骨格とする化合物の化学）という学問は、まず実験してみないと判らないという面白さがあります。私は、医薬品の製造に役立つような新しい有機化学反応を見つける、とくに入手容易な原材料を温和な条件で簡単に高付加価値の製品を作り出せるような方法を開発する研究を進めています。「有機蛍光物質の合成研究」というテーマでSEEDSの体感科学研究に参加し、受講生の皆さんと楽しく実験に挑戦しました。
                        　「何か新しいものを発見・発明したい」という気持ちを持っている生徒さんに　SEEDSへ来てほしいと思っています。大学の研究には答えがありません。いかに自分で考えて道を切り拓くか「考える力」を養ってください！</p></div>
                    </div>
                    <div class="arakawa teachers collapse" id="arakawa">
                        <div class="introduction container-fluid">
                            <div class="row">
                                <div class="teacherphoto col-md-2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/arakawa.png"></div>
                                <div class="col-md-10">
                                    <p class="katagaki">大阪大学理学研究科</p>
                                    <p class="teacher-name">荒川智紀先生</p>
                                    <p>「標準研究の重要性とその物理 -超伝導の不思議な性質-」</p>
                                </div>
                            </div>
						</div>
                        <div class="message"><p>僕は固体中における電子の運動をどのようにすれば制御できるかについて研究しています。人間の目では見ることのできない電子1つ1つの運動をコントロールしたり、その電子同士の関わり合いを直接観測したりすることが大きな魅力です。
                        　「手を動かして実験を進める気を持っている人」、「予測不可能なことが起きても柔軟に対応できる人」、「ノリの良い人(笑)」、「勉強が苦手でもやる気がある人」こんな高校生とぜひ研究してみたいです！様々な科学に関する情報が混同している昨今ですが、自分にとって必要な情報をうまく取捨選択し、SEEDSプログラムで最先端の科学を体感し、実感して自分の好きな研究をやってください！</p></div>
                    </div>
                    
                </div>
                <div class="sns container-fluid">
                    <div class="row">
                        <div class="sns-content facebook col-xs-12 col-md-6">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F%E5%A4%A7%E9%98%AA%E5%A4%A7%E5%AD%A6-SEEDS-1746503512238521%2F&tabs=timeline&width=450px&height=250px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="450px" height="250px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </div>
                        <div class="sns-content twitter col-xs-12 col-md-6">
                            <a class="twitter-timeline" width="450px" height="250px" data-chrome="nofooter" href="https://twitter.com/OSAKASEEDS?ref_src=twsrc%5Etfw">Tweets by OSAKASEEDS</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                </div>
			</div>
			<!--トップページデザイン終わり-戸野-->
            
			<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/content', get_post_format() );

				endwhile;

				the_posts_pagination([
					'prev_text' => twentyseventeen_get_svg( [ 'icon' => 'arrow-left' ] ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( [ 'icon' => 'arrow-right' ] ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
                ]);

			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>
		</main><!-- #main -->



	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
